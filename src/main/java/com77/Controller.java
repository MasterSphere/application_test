package com77;

import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Controller
 * @author: xutao
 * @createDate: 2023-04-14
 * @version: 1.0
 */
@RestController
public class Controller {
    public String test(){
        int i = 10;
        System.out.println("success");
        List<String> listString = new ArrayList<>();
        listString.add(String.valueOf(33));
        return "listString";
    }
}
